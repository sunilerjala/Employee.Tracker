const mongoose = require('mongoose');

const eventsSchema = mongoose.Schema({
    empId:{
        type:String,
        require:true
    },
    eventDate:{
        type:String,
        require:true
    },
    eventDesc:{
        type:String,
        require:true
    }
});

const Events = module.exports = mongoose.model('Events', eventsSchema);
