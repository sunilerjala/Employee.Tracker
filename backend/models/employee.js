const mongoose = require('mongoose');

const employeeSchema = mongoose.Schema({
    'empName': {
        type: String,
        required: true
    },
    'empId':{
        type:String,
        required:true
    },
    'empContactNo':{
        type:Number,
        required:true
    },
    'empEmail': {
        type: String,
        required: true
    },
    'empPassword': {
        type: String,
        required: true
    },
    'empType':{
        type:String,
        default:'employee'
    },
    'empProject':{
        type:String,
        default:null
    },
    'verified': {
        type: Boolean,
        default: false
    }
});


const Employee = module.exports = mongoose.model('Employee', employeeSchema);