const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

const Employee = require('../models/employee');

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('JWT');
opts.secretOrKey = 'secret';

passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
    console.log('jwt_payload: ', jwt_payload);
    Employee.findOne({ id: jwt_payload.sub },{empPassword:0}, function (err, employee) {
        if (err) {
            return done(err, false);
        }
        if (employee) {
            return done(null, employee);
        } else {
            return done(null, false);
        }
    });
}));

passport.serializeUser(function (employee, done) {
    done(null, employee._id);
});