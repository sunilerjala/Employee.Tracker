const express = require('express');
const bodyParser = require('body-parser');
const path = require('path'); 
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const passport = require('passport');

const app = express();


// Mongoose setup
const dbConfig = require('./config/database');

// connect to mongodb using mlab
mongoose.connect(dbConfig.database);
var db = mongoose.connection;
db.on('error', (err) => {
    console.log(err);
})
db.once('open', () => {
    console.log("Connected to database ...");
})


// parse application/x - www - form - urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// cookie parser middleware
app.use(cookieParser());

// handle cross original requests
app.use(cors());

// Passport
app.use(passport.initialize());
app.use(passport.session());
const passportSetup = require('./config/passport');


const authRoutes = require('./routes/auth');
app.use('/auth', authRoutes);
const dashboardRoutes = require('./routes/dashboard');
app.use('/dashboard', dashboardRoutes);

// sets port 3000 to default or unless otherwise specified in the environment
app.set('port', process.env.PORT || 3000);

// Listen
app.listen(app.get('port'), () => {

    console.log('Server started on port : ' + app.get('port'));

});