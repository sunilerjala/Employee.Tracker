const router = require('express').Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

const Employee = require('../models/employee');
const Events = require('../models/events');

function isAuthenticate(req, res, next) {

    var new_var = jwt.verify(req.headers.authorization.split(" ")[1], 'secret');
    req.employee = new_var;
    next();

}



router.get('/employees', isAuthenticate, (req, res) => {

    let type = req.employee.empType;
    
    
    if(type === 'admin') {
        Employee.find({empType: 'employee'},{empPassword:0}, (err, employees) => {
            if(err) throw err;
            res.json(employees);
        });
    }
    else if(type === 'user') {
        res.json(req.employee);
    }

});


router.get('/employeeList', (req, res) => {

    let type = 'admin';
    
    
    if(type === 'admin') {
        Employee.find({empType: 'employee'},{empPassword:0}, (err, employees) => {
            if(err) throw err;
            res.json(employees);
        });
    }
    else if(type === 'user') {
        res.json(req.employee);
    }

});




router.get('/user/:id', isAuthenticate, (req, res) => {

    let userid = req.params.id;

    User.findById(userid, (err, user) => {
        if(err) throw err;
        if(user) {
            res.json({
                success: true,
                user: user
            });
        } else {
            res.json({
                success: false,
                msg: 'No such user exists'
            });
        }
    });

});



router.put('/employee/:id', isAuthenticate, (req ,res) => {
    if(req.employee) {
        let updates = {};

        if(req.body.empEmail) updates.empEmail = req.body.empEmail;
        if(req.body.empName) updates.empName = req.body.empName;
        if (req.body.empType) updates.empType = req.body.empType;
        if (req.body.empProject) updates.empProject = req.body.empProject;
        if (req.body.empContactNo) updates.empContactNo = req.body.empContactNo;
        if (req.body.empId) updates.empId = req.body.empId;
        

        if(updates) {
            Employee.findByIdAndUpdate(req.params.id, updates, {new: true}, (err, employee) => {
                if(err) throw err;
                res.json({
                    success: true,
                    employee: employee
                });
            });
        } else {
            res.json({
                success: false,
                msg: 'bad Request'
            });
        }
    }
   
    
});




router.delete('/user/:id', isAuthenticate, (req, res) => {

    if(req.user) {
        User.deleteOne({_id: req.params.id}, (err, data) => {
            if(err) throw err;
            res.json(user);
        });
    }

})


router.get('/events/:id',isAuthenticate,(req,res) => {
    // let userid = req.params.id;
    let empId = req.params.id;
    Events.find({ empId: { $in : [empId]} }, (err, events) => {
        if(err) throw err;
        if(events) {
            res.json(events);
        } else {
            res.json({
                success: false,
                msg: 'No Event exists'
            });
        }
    });
})




router.post('/event',isAuthenticate,(req,res)=> {
    var new_event = new Events()
    new_event.empId = req.body.empId
    new_event.eventDate = req.body.eventDate
    new_event.eventDesc = req.body.eventDesc
    new_event.save((err, events) => {
        if (err) throw err;
        else {
            res.json({
                success: true,
                events: events
            });
        }
    });
})




router.delete('/employee/:id',isAuthenticate,(req,res)=>{
    if(req.employee) {
        Employee.deleteOne({_id: req.params.id}, (err, data) => {
            if(err) throw err;
            res.json({
                success: true,
                msg: 'Employee Deleted'
            });
        });
    }
})




router.put('/editevent/:id', isAuthenticate, (req ,res) => {
    if(req.employee) {
        let updates = {};

        if(req.body.empId) updates.empId = req.body.empId;
        if(req.body.eventDate) updates.eventDate = req.body.eventDate;
        if (req.body.eventDesc) updates.eventDesc = req.body.eventDesc;
        
        if(updates) {
            Events.findByIdAndUpdate(req.params.id, updates, {new: true}, (err, events) => {
                if(err) throw err;
                res.json({
                    success: true,
                    employee: events
                });
            });
        } else {
            res.json({
                success: false,
                msg: 'bad Request'
            });
        }
    }
   
    
});



module.exports = router;