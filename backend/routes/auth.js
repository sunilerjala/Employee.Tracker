const router = require('express').Router();
const jwt = require('jsonwebtoken');
const randToken = require('rand-token');
const bcrypt = require('bcryptjs');
const passport = require('passport');
const nodemailer = require('nodemailer')
const Employee = require('../models/employee');


// EMAIL TRANSPORTER
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'esunil.iiitbasar@gmail.com',
        pass: 'icui4u131'
    }
})

// Employee Registration
router.post('/register', (req, res) => {

    var new_employee = new Employee();

    new_employee.empName = req.body.name;
    new_employee.empId = req.body.empId;
    new_employee.empContactNo = req.body.contactNo;
    new_employee.empEmail = req.body.email;
    new_employee.empPassword = req.body.password;
    new_employee.empType = req.body.role;
    new_employee.empProject = req.body.project;
    
 
    

    Employee.findOne({empId: new_employee.empId}, (err, employee) => {
        if(err) throw err;
        if(!employee) {
            bcrypt.genSalt(10, (err, salt) => {
            
                bcrypt.hash(new_employee.empPassword, salt, (err, hash) => {
                    if (err) throw err;
                    new_employee.empPassword = hash;
                    new_employee.save((err, employee) => {
                        if (err) throw err;
                        else {
                            var mailOptions = {
                                from: 'esunil.iiitbasar@gmail.com',
                                to: new_employee.empEmail,
                                subject: 'Welcome to Vitwit.',
                                text: 'HI, ' + employee.email + '. Welcome to my localhost'
                            }

                            transporter.sendMail(mailOptions, (err, info) => {
                                if (err) throw err;
                            })
                            res.json({
                                
                                success: true,
                                employee: employee
                            });
                        }
                    });
                });
            });
        } else {
            res.json({
                success: false,
                msg: 'User with this email already exists'
            });
        }
    });

});





// Employee Authentication

router.post('/login', (req ,res) =>{

    var empEmail = req.body.email;
    var empPassword = req.body.password;

    Employee.findOne({ empEmail : empEmail }, (err, employee) => {
        if(err) throw new Error();
        if(!employee) {
            res.json({
                success: false,
                msg: 'No Employee  with this email exists'
            });
        } else {
            bcrypt.compare(empPassword, employee.empPassword, (err, isMatch) => {
                if(err) throw new Error();
                if(isMatch) {

                    const token = jwt.sign(employee.toObject(), 'secret', { expiresIn: 30000000000 });
                    res.json({
                        success: true,
                        employee: employee,
                        token: `JWT ${token}`
                    });
                }
                else {
                    res.json({
                        success: false,
                        msg: 'Wrong password'
                    });
                }
            });
        }
    })

});


module.exports = router;