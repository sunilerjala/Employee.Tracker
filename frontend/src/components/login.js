import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import { loginEmployee } from '../actions/action_login';

class LoginPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            error: ''
        }

        this.handleLogin = this.handleLogin.bind(this);

        this.onEmailChange = this.onEmailChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);

        this.showError = this.showError.bind(this);
    }

    onEmailChange(event) {
        this.setState({
            email: event.target.value
        });
    }
    onPasswordChange(event) {
        this.setState({
            password: event.target.value
        });
    }
    
    showError(msg) {
        document.getElementById('err-row').innerHTML = msg;
        document.getElementById('err-row').style.background = '#ff5555';
        document.getElementById('err-row').style.color = 'white';
        document.getElementById('err-row').style.padding = '20px';
    }
    handleLogin(event){
        event.preventDefault();
        if(this.state.email == '' || this.state.password == '') {
            this.showError('All fields are compulsary');
        }
        else {
            this.props.registerCall(this.state);
        }
        
    }

    

    render() {
        return (
            <div className="container-fluid login-container">
                <div className="row" id="err-row"></div>
                <div className="row header"> Login or <Link to="/home" className = "link"> Go Home</Link></div>
                <form onSubmit={this.handleLogin}>
                    <div className="form-group">
                        <label>Email address</label>
                        <input type="email" 
                        className="form-control" 
                        id="exampleInputEmail1" 
                        placeholder="Enter email" 
                        value={this.state.email} 
                        onChange={this.onEmailChange}/>
                            
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" 
                        className="form-control" 
                        id="exampleInputPassword1" 
                        placeholder="Password"
                        value={this.state.password} 
                        onChange={this.onPasswordChange} />
                    </div>
                    <button type="submit" className="btn btn-primary">Log In</button>
                </form>

                <div className="row orReg">Or <Link to="/register" className="link">Register</Link></div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ registerCall: loginEmployee }, dispatch);
}


function mapStateToProps(state) {
    if (isPromise(state.loginResponse)) {
        state.loginResponse.then(function (res) {
            if(res.data.success) {
                console.log(res.data)
                localStorage.setItem('token', res.data.token);
                localStorage.setItem('employee', JSON.stringify(res.data.employee));
                window.location.href = '/dashboard';
            } else {
                let elem = document.getElementById('err-row');
                elem.innerHTML = res.data.msg;
                elem.style.background = '#ff5555';
                elem.style.color = 'white';
                elem.style.padding = '20px';
            }
        });
    }
    return { loginResponse: state.loginResponse }
}

function isPromise(object) {
    if (Promise && Promise.resolve) {
        return Promise.resolve(object) == object;
    } else {
        throw "Promise not supported in your environment"
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);