import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class HomePage extends Component {
  render() {
    
    return (
      
      <div className="container-fluid home-container">
        <div className='row'>
          <div className="col-md-3"><Link to='/login' className = "btn btn-secondary">Login</Link></div>
          <div className="col-md-3"><Link to='/register' className = "btn btn-secondary">Register</Link></div>
        </div>     
      </div>
      
      // <div className="well">google is very</div>
    );
  }
  }