import React from 'react';
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import Icon from '@material-ui/core/Icon';

export default class Listevents extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            eventId:''
        }
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(event) {
        event.preventDefaults();
        this.setState({
            eventId:event.target.value
        })
        console.log(this.state.eventId)
    }

    render() {
        let events = this.props.events;    
        console.log('these are evetns',events)    
        if(this.props.employeeRole == 'employee') {
            return <p>You are a loser. You have no rights. Here , <Link to={{ pathname: '/edit/' + children._id, state: { user: children, same: true }}}>Edit</Link> youself. </p>
        } else {
            // let newChildren = this.renderRole(children);
            return (
                <div className="panel-body">
                <table className="table table-stripe">
            <thead>
                <tr>
                  <th>Date</th>
                  <th>Description</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                    {events.map(function (event) {

                        return <tr key={event._id}>
                                <td>{event.eventDate}</td>
                                <td>{event.eventDesc}</td>
                                <Link  to = {
                                    {
                                        pathname: '/eventedit/' + event._id,
                                        state: {event: event, same: false}
                                    }
                                }>
                                <Icon  color="primary">
                                edit
                                </Icon>
                                
                                </Link>

                                

                                <Link  style={{marginLeft:'5px'}} to = {
                                    {
                                        pathname:'/eventdelete/'+event._id,
                                        // state:{employee:employee,same:false}
                                    }
                                }>
                                <Icon  color="secondary">
                                delete_forever
                                </Icon>
                                </Link>
                                
                                </tr>
                            
                    })}
                    </tbody>
            </table>
            </div>
               
            )
        }
    }

   

    
}



