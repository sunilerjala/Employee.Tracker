import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { getEvents } from '../actions/action_dashboard';
import { addEvent } from '../actions/action_dashboard';
import { deleteEvent } from '../actions/action_dashboard';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
// import DatePicker from 'react-date-picker';
import Listevents from './listevents';
import Icon from '@material-ui/core/Icon';

class Events extends React.Component {
    constructor(props){
        super(props);
        
        

        if(!localStorage.getItem('token') || !localStorage.getItem('employee')) {
            window.location.href = '/login';
        }

        this.state = {
            
            eventDate:moment(),
            eventDesc:'',
            employee: JSON.parse(localStorage.getItem('employee')),
            employeeRole: JSON.parse(localStorage.getItem('employee')).role,
            empId:JSON.parse(localStorage.getItem('employee'))._id,
            employeeID: (this.props.match.params.id) ? this.props.match.params.id : null,
            eventEmployeeId:( this.props.location.state.employee.empName ) ? this.props.location.state.employee._id :this.props.location.state.employee.empId,
            events: [],
        };

        this.onDateChange = this.onDateChange.bind(this);
        this.onDescChange = this.onDescChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.showError = this.showError.bind(this);

    }

     componentDidMount() {
        
         this.props.getEvents(this.state.eventEmployeeId);    
    }


    componentWillReceiveProps(props) {
        props.dashboardResponse.then(data => {
            this.setState({ events: data.data});
        })
        
    }

    logout(){
        localStorage.removeItem('token');
        localStorage.removeItem('employee');
        window.location.href = '/home';
    }

    showError(msg) {
        document.getElementById('err-row').innerHTML = msg;
        document.getElementById('err-row').style.background = '#ff5555';
        document.getElementById('err-row').style.color = 'white';
        document.getElementById('err-row').style.padding = '20px';
    }


    onDescChange(event) {
        this.setState({
            eventDesc:event.target.value
        });
    }

    onDateChange(date) {
        
        this.setState({
            eventDate:date
        })
    }



    handleSubmit(event) {
        event.preventDefault();
        const data = {
            empId:this.state.eventEmployeeId,
            eventDate:this.state.eventDate.format('L'),
            eventDesc:this.state.eventDesc
        }
        
        if(this.state.empId == '' || this.state.eventDate == '' || this.state.eventDesc == '') {
            this.showError('All fields are compulsary');
        }
        else {
           
            this.props.addEvent(data);
            window.location.reload()
        }
        

    }

    render() {
        return(

            <div className = "dashboard-wrapper container-fluid outer">
                <div style={{textAlign:'center'}} className="dashboard-header" >VitWit-Events
                <a href="javascript:void(0)" style={{float:'right'}} className = "btn btn-danger" onClick={this.logout}>Logout</a>
                <Link to = "/dashboard" style={{marginRight:'10px',float:'right'}} className = "btn btn-danger">Home</Link>
                </div>
                <div className="container current-user">
                    {/* <div className="row"><h5>CURRENT USER: </h5> </div> */}
                    {/* <div className="row">Date : {this.state.events}</div> */}
                    {/* <div className="row">Description : {this.state.employee.empName}</div> */}
                    <div>
                    <div className="row" style={{marginBottom:'10px'}} id="err-row"></div>

                    <form onSubmit = {this.handleSubmit} id="register-form">


                    <div className="form-group">
                        <label>Date</label>
                        {/* <input type="text"
                            className="form-control"
                            placeholder="dd/mm/yyyy"
                            value={this.state.eventDate}
                            onChange={this.onDateChange} /> */}
                            <DatePicker
                            className="form-control"
                            selected={this.state.eventDate}
                            onChange={this.onDateChange}
                            value={this.state.eventDate}
                        />
                    </div>

                    <div className="form-group">
                        <label>Description</label>
                        <input type="textarea"
                            className="form-control"
                            placeholder="Enter Description"
                            value={this.state.eventDesc}
                            onChange={this.onDescChange} />

                    </div>

                    
                    <button type = "submit" className = "btn btn-primary">Add Event</button>
                    
                    {/* <Link to = "/dashboard" style={{marginLeft:'10px'}} className = "btn btn-primary">Go Home</Link> */}
                       
                    </form>
                    

                    </div>
                    {/* <div className="row"><small>(1: Super Admin, 2: Admin, 3: User)</small></div> */}
                    {/* <div className="row"><Link  className = "edit-user-btn" to={{ pathname: '/edit/' + this.state.employee._id, state: {employee: this.state.employee, same: true}}}>Edit Profile</Link></div>
                    <div className="row"><Link  className = "edit-user-btn" to={{ pathname: '/events/' + this.state.employee._id }}>View Events</Link></div> */}
                    
                    <Listevents className = "container list-container" events={this.state.events} employeeRole = {this.state.employeeRole}></Listevents>
                </div>

                {/* <List className = "container list-container" employees={this.state.employees} employeeRole = {this.state.employeeRole}></List> */}
            </div>
            
        )
    }

}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getEvents,addEvent }, dispatch);
}

function mapStateToProps(state) {
    return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(Events);