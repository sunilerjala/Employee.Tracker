import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { registerEmployee } from '../actions/action_register';




class RegisterPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            role: 'employee',
            name: '',
            empId:'',
            contactNo:'',
            project:'A',
            picture:null
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onRoleChange = this.onRoleChange.bind(this);
        this.onEmpIdChange = this.onEmpIdChange.bind(this);
        this.onContactChange = this.onContactChange.bind(this);
        this.onProjectChange = this.onProjectChange.bind(this);
        this.showError = this.showError.bind(this);
        
    }

    showError(msg) {
        let elem = document.getElementById('err-row');
        elem.innerHTML = msg;
        elem.style.background = '#ff5555';
        elem.style.color = 'white';
        elem.style.padding = '20px';
    }

    handleSubmit(event) {
        event.preventDefault();

        
        if (this.state.email == '' || this.state.project == '' || this.state.empId == '' ||this.state.password == '' || this.state.role == '' || this.state.name == '' ) {
            this.showError('All fields are compulsary');
        } else {
            this.props.registerCall(this.state);
        }
    }
    onEmailChange(event){
        this.setState({
            email: event.target.value
        });
    }
    onNameChange(event) {
        this.setState({
            name: event.target.value
        });
    }
    onPasswordChange(event) {
        this.setState({
            password: event.target.value
        });
    }
    onRoleChange(event){
        this.setState({
            role: event.target.value
        });
    }
    onContactChange(event) {
        this.setState({
            contactNo:event.target.value
        });
    }
    onEmpIdChange(event) {
        this.setState({
            empId:event.target.value
        });
    }
    onProjectChange(event) {
        this.setState({
            project:event.target.value
        })
    }

   

    // componentWillReceiveProps(new_props) {
    //     console.log(this.new_props.user);
    // }

    render() {
        return(
            <div className="container-fluid register-container">
            <div className="row" id="err-row"></div>
            <div className="row header">Register or <Link to="/home" className="link"> Go Home</Link></div>
                <form onSubmit = {this.handleSubmit} id="register-form">


                    <div className="form-group">
                        <label>Name</label>
                        <input type="text"
                            className="form-control"
                            placeholder="Name"
                            value={this.state.name}
                            onChange={this.onNameChange} />
                    </div>

                    <div className="form-group">
                        <label>Email address</label>
                        <input type="email"
                            className="form-control"
                            placeholder="Enter email"
                            value={this.state.email}
                            onChange={this.onEmailChange} />

                    </div>
            
                    {/* <div className="form-group" >
                    
                    <img src={require('../images/home.jpeg')} />

                    </div> */}


                    <div className="form-group">
                        <label>Employee ID</label>
                        <input type="text"
                            className="form-control"
                            placeholder="Enter EmployeeId"
                            value={this.state.empId}
                            onChange={this.onEmpIdChange} />

                    </div>

                    

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password"
                            className="form-control"
                            placeholder="Password"
                            value={this.state.password}
                            onChange={this.onPasswordChange} />
                    </div>

                    <div className="form-group">
                        <label>Contact No</label>
                        <input type="number"
                            className="form-control"
                            placeholder="Contact No"
                            value={this.state.contactNo}
                            onChange={this.onContactChange} />

                    </div>

                    <div className="form-group">
                        <label>Role</label>

                        <select className="form-control" value={this.state.role} onChange = {this.onRoleChange}>
                            <option value="employee">Employee</option>
                            <option value="admin">Admin</option>  
                        </select>

                    </div>

                    <div className="form-group">
                        <label>Project</label>
                        <select className="form-control" value={this.state.project} onChange = {this.onProjectChange}>
                            <option value="A">A</option>
                            <option value="B">B</option>  
                        </select>
                    </div>
        
                    <button type = "submit" className = "btn btn-primary">Register</button>
                </form>
                <div className="row orReg">Or <Link to="/login" className="link"> Login</Link></div>
            </div>
        )
    }

}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({ registerCall: registerEmployee }, dispatch);
}

function mapStateToProps(state){
    if(isPromise(state.registerResponse)){
        state.registerResponse.then(function(res){
           if(res.data.success) {
            window.location.href = '/login';
           } else {
               let elem = document.getElementById('err-row');
               elem.innerHTML = res.data.msg;
               elem.style.background = '#ff5555';
               elem.style.color = 'white';
               elem.style.padding = '20px';
           }
        });
    }
    return { registerResponse: state.registerResponse }    
}

function isPromise(object) {
    if (Promise && Promise.resolve) {
        return Promise.resolve(object) == object;
    } else {
        throw "Promise not supported in your environment"
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);