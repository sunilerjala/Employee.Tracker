import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { updateEmployee } from '../actions/action_dashboard';
import { deleteEmployee } from '../actions/action_dashboard';

class EditComponent extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            curEmp :(!!localStorage.getItem('employee'))?JSON.parse(localStorage.getItem('employee')):null,
            employee: (this.props.location.state) ? this.props.location.state.employee : null,
            employeeID: (this.props.match.params.id) ? this.props.match.params.id : null,
            
            empEmail: (this.props.location.state) ? this.props.location.state.employee.empEmail : null,
            empName: (this.props.location.state) ? this.props.location.state.employee.empName : null,
            empType: (this.props.location.state) ? this.props.location.state.employee.empType : null,
            empProject:(this.props.location.state)?this.props.location.state.employee.empProject:null,
            empContactNo:(this.props.location.state)?this.props.location.state.employee.empContactNo:null,
            empId:(this.props.location.state)?this.props.location.state.employee.empId:null,
            same: false,
            error: '',
            update: {}
        }


        
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        // this.onTypeChange = this.onTypeChange.bind(this);
        this.onProjectChange = this.onProjectChange.bind(this);
        this.onContactNoChange = this.onContactNoChange.bind(this);
        this.onempIdChange = this.onempIdChange.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.showError = this.showError.bind(this);
        this.deleteEmployee = this.deleteEmployee.bind(this);

        // this.checkSame = this.checkSame.bind(this);
    }

    componentDidMount() {
        
        // if (this.state.role == 'SUPERADMIN') this.setState({role: 1});
        // else if (this.state.role == 'ADMIN') this.setState({ role: 2 });
        // else if (this.state.role == 'USER') this.setState({ role: 3 });

        if (this.props.location.state && this.props.location.state.same) {
            this.setState({ same: true });
        }
    }



    componentWillReceiveProps(props) {
        
        var isSame = this.state.same;
        props.dashboardResponse.then(function(response){
            if(response.data.success) {
                if (isSame) {
                    localStorage.setItem('employee', JSON.stringify(response.data.employee));
                }
                window.location.href = "/dashboard";
            } else {
                this.setState({
                    error: response.data.msg
                });
            }
        });
    }



    onEmailChange(event) {
        this.setState({
            empEmail:event.target.value
        })
    }

    onNameChange(event) {
        this.setState({
            empName: event.target.value
        });
    }

    // onTypeChange(event) {
    //     this.setState({
    //         empType: event.target.value
    //     });
    // }

    onContactNoChange(event) {
        this.setState({
           empContactNo:event.target.value 
        })
    }
    onProjectChange(event) {
        this.setState({
            empProject:event.target.value
        })
    }
    onempIdChange(event) {
        this.setState({
            empId:event.target.value
        })
    }
    logout(){
        localStorage.removeItem('token');
        localStorage.removeItem('employee');
        window.location.href = '/home';
    }

    showError(msg) {
        document.getElementById('err-row').innerHTML = msg;
        document.getElementById('err-row').style.background = '#ff5555';
        document.getElementById('err-row').style.color = 'white';
        document.getElementById('err-row').style.padding = '20px';
    }

    handleUpdate(event) {
        event.preventDefault();
        const data = {
            empEmail: this.state.empEmail,
            empName: this.state.empName,
            empType: this.state.empType,
            empContactNo:this.state.empContactNo,
            empProject:this.state.empProject,
            empId:this.state.empId
            
        }
        if(this.state.empEmail == '' || this.state.empName == '' || this.state.empType == '' || this.state.empContactNo== '' || this.state.empContactNo == ''  || this.state.empId == '') {
            this.showError('All fields are compulsary');
        }
        else {
            this.props.updateEmployee(this.state.employeeID, data);
        }

        
    }

    deleteEmployee(event) {
        event.preventDefault();
        console.log('front end delete employee')
        this.props.deleteEmployee(this.state.employeeID)
    }

    render() {

        if(this.state.employee && this.state.employeeID) {
            
            return (
                <div className = "dashboard-wrapper container-fluid outer">
                
                {/* <p style={{textAlign:'center'}} className="dashboard-header" >VitWit-Edit-Profile </p>
                <div className="row" id="err-row"></div> */}


                <div style={{textAlign:'center'}} className="dashboard-header" >VitWit-Edit-Profile
                <a href="javascript:void(0)" style={{float:'right'}} className = "btn btn-danger" onClick={this.logout}>Logout</a>
                <Link to = "/dashboard" style={{marginRight:'10px',float:'right'}} className = "btn btn-danger">Home</Link>
                </div>

                

                {/* <a href="javascript:void(0)" className = "btn btn-danger" onClick={this.logout}>Logout</a> 
                <Link to = "/dashboard" style={{marginLeft:'5px'}} className = "btn btn-primary">Cancel</Link> */}
                <div className='container edit-container'>
                
                {/* <div className = 'row header'><h2>Edit Employee</h2></div> */}
                    <form onSubmit = {this.handleUpdate}>
                    <div className="form-group" id="err-row"></div>
                        <div className="form-group">
                            <label>Email address</label>
                            <input type="email"
                                className="form-control"
                                id="edit-email"
                                value={this.state.empEmail}
                                onChange={this.onEmailChange} />
                        </div>

                        <div className="form-group">
                            <label>Name</label>
                            <input type="text"
                                className="form-control"
                                id="edit-name"
                                value={this.state.empName}
                                onChange={this.onNameChange} />
                        </div>

                        <div className="form-group">
                            <label>Contact No</label>
                            <input type="text"
                                className="form-control"
                                id="edit-name"
                                value={this.state.empContactNo}
                                onChange={this.onContactNoChange} />
                        </div>

                        <div className="form-group">
                            <label>Project</label>
                            <input type="text"
                                className="form-control"
                                id="edit-name"
                                value={this.state.empProject}
                                onChange={this.onProjectChange}
                                disabled={!(this.state.curEmp.empType.toString() === 'admin')}
                                 />
                        </div>

                        <div className="form-group">
                            <label>Employee ID</label>
                            <input type="text"
                                className="form-control"
                                id="edit-name"
                                value={this.state.empType}
                                onChange={this.onempIdChange}
                                disabled={!(this.state.curEmp.empType.toString() === 'admin')}
                             />
                        </div>
                        
                        
                        {/* <div className="form-group">
                            <label>Emp Type</label>
                            <select value={this.state.empType} onChange={this.onTypeChange}>
                                <option value="user">User</option>
                                <option value="admin" disabled>Admin</option>
                            </select>
                        </div> */}

                        <div className="row">
                            <div className="col-md-2">
                                <button type="submit" className = "btn btn-primary">Update</button>
                            </div>

                            <div className="col-md-2">

                                <button  className = "btn btn-danger" onClick = {this.deleteEmployee}>Delete</button>

                            </div>

                            
                            <div className="col-md-2">
                                <Link to = "/dashboard" className = "btn btn-primary">Cancel</Link>
                            </div>
                        </div>
                    </form>
                    <div className="row">{this.state.error}</div>
                </div>
                </div>
            )
        } 
        else {
            return <p className = "err">You are Unauthorized<Link to = "/dashboard" style={{marginLeft:'10px'}} className = "btn btn-primary">Go Home</Link></p>

        }
    }

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ updateEmployee,deleteEmployee }, dispatch);
}

function mapStateToProps(state) {
    return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(EditComponent);