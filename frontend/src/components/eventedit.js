import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Router } from 'react-router'

import { updateEvent } from '../actions/action_dashboard';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

var browserHistory = Router.browserHistory;

class eventEditComponent extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            curEmp :(!!localStorage.getItem('employee'))?JSON.parse(localStorage.getItem('employee')):null,
            // employee: (this.props.location.state) ? this.props.location.state.employee : null,
            // employeeID: (this.props.match.params.id) ? this.props.match.params.id : null,
            curEmpId : (!!localStorage.getItem('employee'))?JSON.parse(localStorage.getItem('employee'))._id:null,
            // eventId:(this.props.match.params.id)?this.props.match.param.id : null,
            event:(this.props.location.state) ? this.props.location.state.event:null,
            eventDate:(this.props.location.state) ? moment((this.props.location.state.event.eventDate).toString()):null,

            eventDesc:(this.props.location.state) ? this.props.location.state.event.eventDesc:null,
            eventID:(this.props.location.state) ? this.props.location.state.event._id:null,
            empId:(this.props.location.state) ? this.props.location.state.event.empId:null,
            
            same: false,
            error: '',
            update: {}
        }


        
        this.onDateChange = this.onDateChange.bind(this);
        this.onDescChange = this.onDescChange.bind(this);
        
        this.handleUpdate = this.handleUpdate.bind(this);
        this.logout = this.logout.bind(this);
        // this.checkSame = this.checkSame.bind(this);
    }

    componentDidMount() {
        
        // if (this.state.role == 'SUPERADMIN') this.setState({role: 1});
        // else if (this.state.role == 'ADMIN') this.setState({ role: 2 });
        // else if (this.state.role == 'USER') this.setState({ role: 3 });
        console.log(this.state.eventDate)

        if (this.props.location.state && this.props.location.state.same) {
            this.setState({ same: true });
        }
        
    }



    componentWillReceiveProps(props) {
        
        var isSame = this.state.same;
        props.dashboardResponse.then(function(response){
            if(response.data.success) {
                if (isSame) {
                    localStorage.setItem('employee', JSON.stringify(response.data.employee));
                }
                window.alert('Event Updated Please Go back with Cancel');
            } else {
                this.setState({
                    error: response.data.msg
                });
            }
        });
    }



    onDateChange(date) {
        this.setState({
            eventDate: date
        });
    }

    onDescChange(event) {
        this.setState({
            eventDesc: event.target.value
        });
    }

    logout(){
        localStorage.removeItem('token');
        localStorage.removeItem('employee');
        window.location.href = '/home';
    }

    handleUpdate(event) {
        event.preventDefault();

        const data = {

            empId:this.state.empId,
            eventDate:this.state.eventDate.format('L'),
            eventDesc:this.state.eventDesc
        }
    //    console.log(this.state.eventDate.format('L'))
        this.props.updateEvent(this.state.eventID, data);
    }

    render() {

        if(this.state.curEmp && this.state.curEmpId) {
            
            return (
                <div className = "dashboard-wrapper container-fluid outer">
                <div style={{textAlign:'center'}} className="dashboard-header" >VitWit-Edit-Event
                <a href="javascript:void(0)" style={{float:'right'}} className = "btn btn-danger" onClick={this.logout}>Logout</a>
                <Link to = "/dashboard" style={{marginRight:'10px',float:'right'}} className = "btn btn-danger">Home</Link>
                </div>
                <div className='container edit-container'>
                
                    <form onSubmit = {this.handleUpdate}>
                        <div className="form-group">
                            <label>Event Date</label>
                            {/* <input type="text"
                                className="form-control"
                                id="edit-name"
                                value={this.state.eventDate}
                                onChange={this.onDateChange} required /> */}

                            <DatePicker
                            className="form-control"
                            selected={this.state.eventDate}
                            onChange={this.onDateChange}
                            value={this.state.eventDate}
                            />
                        </div>

                        <div className="form-group">
                            <label>Event Desc</label>
                            <input type="text"
                                className="form-control"
                                id="edit-name"
                                value={this.state.eventDesc}
                                onChange={this.onDescChange} required />
                        </div>

                        
                

                        <div className="row">
                            <div className="col-md-2">
                                <button type="submit" className = "btn btn-primary">Update</button>
                            </div>
                           
                            <div className="col-md-2">
                                <Link style={{margin:'2px'}} to={{ pathname: '/events/'+this.state.event.empId,state:{employee:this.state.event} }} className = "btn btn-primary">Cancel</Link>
                            </div>
                        </div>
                    </form>
                    <div className="row">{this.state.error}</div>
                </div>
                </div>
            )
        } 
        else {
            return <p className = "err">You are Unauthorized</p>
        }
    }

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ updateEvent }, dispatch);
}

function mapStateToProps(state) {
    return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(eventEditComponent);