import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { getEmployees } from '../actions/action_dashboard';
import List from './list';
import Icon from '@material-ui/core/Icon';

class Dashboard extends React.Component {
    constructor(props){
        super(props);

        if(!localStorage.getItem('token') || !localStorage.getItem('employee')) {
            window.location.href = '/login';
        }
        
        this.state = {
            employee: JSON.parse(localStorage.getItem('employee')),
            employeeRole: JSON.parse(localStorage.getItem('employee')).role,
            employees: [],
            goog:[]
        }
        
    }

     componentDidMount() {
         this.props.getEmployees();    
    }

    componentWillReceiveProps(props) {
        props.dashboardResponse.then(data => {
            this.setState({ employees: data.data});
           
        })
    }

   

    logout(){
        localStorage.removeItem('token');
        localStorage.removeItem('employee');
        window.location.href = '/home';
    }
    
   
    

    render() {
        return(
            
        <div className = "dashboard-wrapper container-fluid outer">
                
            <div style={{textAlign:'center'}} className="dashboard-header" >VitWit-Dashboard
            <a href="javascript:void(0)" style={{float:'right'}} className = "btn btn-danger" onClick={this.logout}>Logout</a>
            </div>
            
            <div style={{width:'100%',margin:'auto',textAlign:'center'}} className="container current-user">
            
        <div className="row profile">
		<div className="col-md-4">
			<div className="profile-sidebar">
				{/* <!-- SIDEBAR USERPIC --> */}
				<div className="profile-userpic">
					<img src="https://2.imimg.com/data2/LQ/QV/MY-/teddy-small-size-500x500.jpg" className="img-responsive" alt="" />
				</div>

				<div className="profile-usertitle">
					<div className="profile-usertitle-name">
                            {this.state.employee.empName}
					</div>
					<div className="profile-usertitle-job">
                        {this.state.employee.empType}
					</div>

                    <div className="profile-usertitle-job">
                        {this.state.employee.empEmail}
					</div>
                    
				</div>
				{/* <!-- END SIDEBAR USER TITLE --> */}
				{/* <!-- SIDEBAR BUTTONS --> */}
				<div className="profile-userbuttons">
                {((this.state.employee.empType) === "employee" && <Link  className = "btn btn-primary" to={{ pathname: '/events/'+this.state.employee._id,state:{employee:this.state.employee} }}>View Events</Link>)}
                <Link  className="btn btn-warning" to={{ pathname: '/edit/' + this.state.employee._id, state: {employee: this.state.employee, same: true}}}>Edit Profile</Link>
				</div>
                
				
			</div>
           
		</div>
        <div className="col-md-8">
        {((this.state.employee.empType) === "admin" && <List className = "container list-container" employees={this.state.employees} employeeRole = {this.state.employeeRole}></List>)}
        </div>
	</div>
    </div>
    </div>

            
            
        )
    }

}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getEmployees }, dispatch);
}

function mapStateToProps(state) {
    return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);