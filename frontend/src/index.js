import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import homepage from './components/homepage';

import LoginPage from './components/login';
import RegisterPage from './components/register';
import Dashboard from './components/dashboard';
import EditComponent from './components/edit';
import EventComponent from './components/events'
import EventEditComponent from './components/eventedit'

import reducers from './reducers';
import { promiseMiddleware } from 'redux-promise';
import Redirect from 'react-router-dom/Redirect';


const createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <Router>
      <Switch>
        {/* <Route exact path = "/" render = {() => {
          <Redirect to="/home/"></Redirect>
        }}></Route> */}
        <Route exact path="/home" component= {homepage}></Route>
        
        <Route path="/login" component={LoginPage}></Route>
        <Route path="/register" component={RegisterPage}></Route>       
        <Route path="/dashboard" component={Dashboard}></Route>
        <Route path="/edit/:id" component={EditComponent}></Route>    
        <Route path="/events" component = {EventComponent}></Route> 
        <Route path="/eventedit/:id" component={EventEditComponent}></Route>                
      </Switch>
    </Router>
  </Provider>
  , document.querySelector('.container-fluid'));
