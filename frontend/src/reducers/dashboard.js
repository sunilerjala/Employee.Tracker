export default function (state = null, action) {

    switch(action.type) {
        case 'GET_EMPLOYEES':
            return action.payload;
        case 'GET_EMPLOYEE':
            return action.payload;
        case 'UPDATE_EMPLOYEE':
            return action.payload;
        case 'DELETE_EMPLOYEE':
            return action.payload;

        case 'ADD_EVENT':
            return action.payload;
        case 'GET_EVENTS':
            return action.payload;
        case 'UPDATE_EVENT':
            return action.payload;
        case 'DELETE_EVENT':
            return action.payload
            
        default:
            return {};        
    }

}