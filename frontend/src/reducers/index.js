import { combineReducers } from 'redux';
import RegisterReducer from './register';
import LoginReducer from './login';
import DashboardReducer from './dashboard'; 

const rootReducer = combineReducers({
  registerResponse: RegisterReducer,
  loginResponse: LoginReducer,
  dashboardResponse: DashboardReducer
});

export default rootReducer;
