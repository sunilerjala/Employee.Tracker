import axios from 'axios';

export const LOGIN_ACTION = 'LOGIN';

export function loginEmployee(data) {

    const URL = 'http://localhost:3000/auth/login';
    const req = axios(URL, {
        method: 'POST',
        data: data
    });
    
    return {
        type: LOGIN_ACTION,
        payload: req
    }

}