import axios from 'axios';

export const GET_USER = 'GET_USER';
export const GET_EMPLOYEES = 'GET_EMPLOYEES';
export const UPDATE_EMPLOYEE = 'UPDATE_EMPLOYEE'
export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE'

export const GET_EVENTS = 'GET_EVENTS'
export const ADD_EVENT = 'ADD_EVENT'
export const UPDATE_EVENT = 'UPDATE_EVENT'
export const DELETE_EVENT = 'DELETE_EVENT'

const token = localStorage.getItem('token');

export function getEmployees() {
    
    const URL = 'http://localhost:3000/dashboard/employees';
    const request = axios(URL, {
        method: 'GET',
        headers: {'Authorization': token},
    })

    return {
        type: GET_EMPLOYEES,
        payload: request
    }


}

export function getUserByID(id) {

    const url = 'http://localhost:3000/dashboard/user/'+ id;
    const request = axios(url, {
        method: 'GET',
        headers: { 'Authorization': token },
    });
    
    return {
        type: GET_USER,
        payload: request
    }
}

export function updateEmployee(employeeid, data) {
    const url = 'http://localhost:3000/dashboard/employee/' + employeeid;
    const request = axios(url, {
        method: 'PUT',
        headers: { 'Authorization': token },
        data: data,
    });
    return {
        type: UPDATE_EMPLOYEE,
        payload: request
    }
}

export function deleteEmployee(employeeid) {
    console.log('comming into delete employee');
    const url = 'http://localhost:3000/dashboard/employee/' + employeeid;
    const request = axios(url,{
        method:'DELETE',
        headers:{'Authorization':token},
    });
    return {
        type:DELETE_EMPLOYEE,
        payload:request
    }

}


export function getEvents(id) {
    console.log('Coming into the getevents');
    const url = 'http://localhost:3000/dashboard/events/'+id;
    const request = axios(url,{
        method:'GET',
        headers:{ 'Authorization': token }
    });
    return {
        type: GET_EVENTS,
        payload: request
    }
}



export function addEvent(data) {
    console.log('coming into add event');
    console.log(data)
    const url = 'http://localhost:3000/dashboard/event';
    const request = axios(url,{
        method:'POST',
        headers:{ 'Authorization': token },
        data:data
    });
    return {
        type:ADD_EVENT,
        payload: request
}}


export function deleteEvent(eventId) {
    const url = 'http://localhost:3000/dashboard/event' + eventId;
    const request = axios(url,{
        method:'DELETE',
        headers:{'Authorization':token},
    });
    return {
        type:DELETE_EVENT,
        payload:request
    }

}


export function updateEvent(eventId,data) {
    console.log('coming into update event')

    const url = 'http://localhost:3000/dashboard/editevent/' + eventId;
    const request = axios(url, {
        method:'PUT',
        headers: { 'Authorization': token },
        data: data,
    });
    return {
        type: UPDATE_EVENT,
        payload: request
    }
}


