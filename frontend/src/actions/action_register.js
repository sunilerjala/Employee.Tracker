import axios from 'axios';

export const REGISTER_ACTION = 'REGISTER';

export function registerEmployee(data) {

    const URL = 'http://localhost:3000/auth/register';
    const req = axios(URL, {
        method: 'POST',
        data: data
    });

    return {
        type: REGISTER_ACTION,
        payload: req
    }

}